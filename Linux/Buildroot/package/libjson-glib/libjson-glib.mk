#############################################################
#
# libjson-glib
#
#############################################################
LIBJSONGLIB_VERSION_MAJOR = 0
LIBJSONGLIB_VERSION_MINOR = 12
LIBJSONGLIB_VERSION_MICRO = 2
LIBJSONGLIB_VERSION = $(LIBJSONGLIB_VERSION_MAJOR).$(LIBJSONGLIB_VERSION_MINOR).$(LIBJSONGLIB_VERSION_MICRO)
LIBJSONGLIB_SOURCE = json-glib-$(LIBJSONGLIB_VERSION).tar.bz2

LIBJSONGLIB_SITE = http://ftp.gnome.org/pub/GNOME/sources/json-glib/$(LIBJSONGLIB_VERSION_MAJOR).$(LIBJSONGLIB_VERSION_MINOR)

LIBJSONGLIB_LIBTOOL_PATCH = NO
LIBJSONGLIB_INSTALL_STAGING = YES
LIBJSONGLIB_INSTALL_TARGET = YES
#LIBJSONGLIB_INSTALL_STAGING_OPT = DESTDIR=$(STAGING_DIR) LDFLAGS=-L$(STAGING_DIR)/usr/lib install


LIBJSONGLIB_CONF_OPT = --enable-shared \
       +		--enable-static

HOST_LIBJSONGLIB_CONF_OPT = \
       +		--enable-shared \
       +		--disable-static \
       +		--disable-gtk-doc \
       +		--enable-debug=no \
       +
LIBJSONGLIB_DEPENDENCIES = libglib2 host-pkg-config

$(eval $(call AUTOTARGETS,package,LIBJSONGLIB))
