//
//  WUTMeasurementsViewController.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTViewController.h"

@class WUTPlotController;

@class SRWebSocket;

@protocol WUTMeasurementsViewControllerDelegate ;

@interface WUTMeasurementsViewController : WUTViewController

@property (weak, nonatomic) id<WUTMeasurementsViewControllerDelegate>delegate;

@property (strong, nonatomic) WUTPlotController *controller;

@property (strong, nonatomic) SRWebSocket *socket;

@end

@protocol WUTMeasurementsViewControllerDelegate <NSObject>

@optional

- (void)measurementsViewControllerDidFinish:(WUTMeasurementsViewController *)viewController;

@end