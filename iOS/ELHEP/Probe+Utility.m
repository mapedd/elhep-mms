//
//  Probe+Utility.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "Probe+Utility.h"
#import "ProbeIn+Utility.h"
#import "ProbeOut+Utility.h"
#import "ProbeActions.h"

@implementation Probe (Utility)

- (NSDictionary *)configuration{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [dictionary setObject:self.probeID forKey:@"probeId"];
    
    if (self.probeIn.wasEdited) {
        NSMutableDictionary *probeSpecDict = [NSMutableDictionary dictionaryWithCapacity:0];
        
        [probeSpecDict setObject:[self.probeIn configuration] forKey:@"probeIn"];
        
        [dictionary setObject:probeSpecDict forKey:@"probeSpec"];
    }
    
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

- (NSString *)description{
    return [NSString stringWithFormat:@"probeId: %@,\nprobeName: %@,\nprobeIn:%@,\nprobeOut:%@,\nprobeActions: %@",
            [self.probeID description],
            self.probeName,
            [self.probeIn description],
            [self.probeOut description],
            [self.probeActions description]];
}

@end
