//
//  NSString+NSData.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 8/22/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "NSString+NSData.h"

@implementation NSString (NSData)

+ (NSString *)stringFromData:(NSData *)data{
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

@end
