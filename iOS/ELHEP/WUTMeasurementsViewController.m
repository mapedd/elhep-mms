//
//  WUTMeasurementsViewController.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTMeasurementsViewController.h"
#import "WUTPlotView.h"
#import "UIView+TKGeometry.h"
#import "WUTPlotController.h"
#import "SRWebSocket.h"

@interface WUTMeasurementsViewController ()

@end

@implementation WUTMeasurementsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"MMS", nil);
    
    UIBarButtonItem *startButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Finish", nil)
                                                                    style:UIBarButtonItemStylePlain 
                                                                   target:self
                                                                   action:@selector(finishCapturingData)];
    self.navigationItem.leftBarButtonItem = startButton;
    
    UIBarButtonItem *pauseButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Pause", nil)
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(pauseCapturingData)];
    self.navigationItem.rightBarButtonItem = pauseButton;
    
    self.view.autoresizesSubviews = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.contentSize = CGSizeMake(self.view.width, self.view.height * 2.0f);
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    scrollView.autoresizesSubviews = YES;
    [self.view addSubview:scrollView];
    
    CGRect frame = self.isIphone ? CGRectMake(0., 10., 320, 200) : CGRectMake(0., 10., 768, 400);
    
    WUTPlotView *plotView = [[WUTPlotView alloc] initWithFrame:frame dataSource:nil];
    plotView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [scrollView addSubview:plotView];
    
    
    _controller = [[WUTPlotController alloc] initWithPlotView:plotView];
    self.socket.delegate = (id<SRWebSocketDelegate>)_controller;
    [_controller startSendingResults];
    
    [[NSNotificationCenter defaultCenter] addObserver:self forKeyPath:@"isSendingResults" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)unloadView{
    self.delegate = nil;
}

#pragma mark - Private

- (void)finishCapturingData{
    [_controller stopSendingResults];
    if ([self.delegate respondsToSelector:@selector(measurementsViewControllerDidFinish:)]) {
        [self.delegate measurementsViewControllerDidFinish:self];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"isSendingResults"]) {
        NSLog(@"is sending:%d ", [[_controller valueForKeyPath:@"isSendingResults"] boolValue]);
    }
}

- (void)pauseCapturingData{
    if (_controller.isSendingResults) {
        [_controller stopSendingResults];
        UIBarButtonItem *pauseButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Resume", nil)
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(pauseCapturingData)];
        self.navigationItem.rightBarButtonItem = pauseButton;
    }
    else{
        [_controller startSendingResults];
        UIBarButtonItem *pauseButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Pause", nil)
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(pauseCapturingData)];
        self.navigationItem.rightBarButtonItem = pauseButton;
    }
}


@end
