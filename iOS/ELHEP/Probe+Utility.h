//
//  Probe+Utility.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "Probe.h"

@interface Probe (Utility)

- (NSDictionary *)configuration;

@end
