//
//  WUTResult.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WUTResult : NSObject

@property (strong, nonatomic) NSDate *timeStamp;

@property (nonatomic) CGFloat value;

/* @property (nonatomic) ENUM TYPE measurmentType */

- (id)initWithValue:(CGFloat)value andTimeStamp:(NSDate *)timeStamp;

@end
