//
//  Device+Utility.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "Device.h"

enum {
    WUTInterfaceUnknown = 0,
    WUTInterfaceBluetooth = 1,
    WUTInterfaceWifi = 2,
};

#define kWUTDeviceConnectedNotification @"kWUTDeviceConnectedNotification"

@interface Device (Utility)

- (NSDictionary *)configuration;

@end
