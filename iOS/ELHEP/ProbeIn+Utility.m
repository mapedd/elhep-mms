//
//  ProbeIn+Utility.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "ProbeIn+Utility.h"

@implementation ProbeIn (Utility)

- (NSDictionary *)configuration{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:0];
    
    [dictionary TKsetObject:self.sampleFreq       forKeyIfNotNil:@"sampleFreq"];
    [dictionary TKsetObject:self.autoTriggerRetry forKeyIfNotNil:@"autoTriggerRetry"];
    [dictionary TKsetObject:self.numberOfSamples  forKeyIfNotNil:@"numberOfSamples"];
    [dictionary TKsetObject:self.triggerLow       forKeyIfNotNil:@"triggerLow"];
    [dictionary TKsetObject:self.triggerHigh      forKeyIfNotNil:@"triggerHigh"];
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

@end


@implementation NSMutableDictionary (ProbeIn)

- (void)TKsetObject:(id)anObject forKeyIfNotNil:(id <NSCopying>)aKey{
    if (anObject && ![anObject isKindOfClass:[NSNull class]]) {
        [self setObject:anObject forKey:aKey];
    }
}

@end