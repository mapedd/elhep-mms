//
//  WUTBLEHandler.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/9/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WUTBLEHandler : NSObject

- (void)startDiscoveryWithServiceUUID:(NSString *)uuidString;

- (void)stopScanning;

@end
