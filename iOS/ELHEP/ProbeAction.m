//
//  ProbeAction.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "ProbeAction.h"
#import "ProbeActions.h"


@implementation ProbeAction

@dynamic actionDesc;
@dynamic actionName;
@dynamic actionParameterType;
@dynamic probeActions;

@end
