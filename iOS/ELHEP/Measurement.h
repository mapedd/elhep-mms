//
//  Measurement.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/21/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Measurement : NSObject

@property (nonatomic, assign) NSTimeInterval timestamp;

@property (nonatomic, assign) CGFloat sample;


+ (Measurement *)measurementWithDict:(NSDictionary *)dict;

@end
