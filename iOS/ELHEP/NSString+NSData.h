//
//  NSString+NSData.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 8/22/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSData)

+ (NSString *)stringFromData:(NSData *)data;

@end
