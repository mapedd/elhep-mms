//
//  ProbeActions.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "ProbeActions.h"
#import "Probe.h"
#import "ProbeAction.h"


@implementation ProbeActions

@dynamic actions;
@dynamic probe;

@end
