//
//  ProbeOut.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Probe;

@interface ProbeOut : NSManagedObject

@property (nonatomic, retain) NSNumber * dataType;
@property (nonatomic, retain) NSString * dataUnit;
@property (nonatomic, retain) NSNumber * defaultDisplayType;
@property (nonatomic, retain) NSNumber * probeType;
@property (nonatomic, retain) Probe *probe;

@end
