//
//  mms_configuration.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/9/12.
//  Copyright (c) 2012 Tomasz Kuzma. All rights reserved.
//

#ifndef ELHEP_mms_configuration_h
#define ELHEP_mms_configuration_h

/* MMS Interface Configuration */

#define MMS_INTERFACE_VERSION "0.1"

/* SSDP Configuration */

#define SSDP_MULTICAST_BREAK_TIME 5.0

#define SSDP_PORT_NUMBER 1900

#define SSDP_MULTICAST_IP_ADDRESS "239.255.255.250"

#define MMS_SSDP_SERVICE_NAME "ELHEP:device:MMS"

/* WebSocket configuration */

#define WEB_SOCKET_PORT 7681

#define WS_SOCKET_PROTOCOL_NAME "elhep-test"

/* Bluetooth configuration */

#define MMS_BLE_SERVICE_UUID "DEADF154-0000-0000-0000-233124123141"

#endif
