#!/bin/sh

echo "deleting old files";

rm $1.pdf ../$1.pdf;

rm -f *.log *.aux *.bbl *.gz *.out *.blg *.lof *.toc *.tdo *.toc *.lot *.fls *.fdb_latexmk;

echo "compiling";

xelatex $1;
bibtex 	$1;
xelatex $1;
xelatex $1;

echo "removing unneeded files";

rm -f *.log *.aux *.bbl *.gz *.out *.blg *.lof *.toc *.tdo *.toc *.lot;

echo "done";