package com.example.elhep_mms;

import java.io.IOException;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

public class mms_bluetooth {

	// TODO currently only one device -> extend to array
	// TODO separate threads for connected devices
	
	private final BluetoothAdapter _bt_adapter;
	private BluetoothDevice _bt_device;
	private BluetoothSocket _bt_socket;
	private mms_bluetooth_device _bt_thread;
	
	public mms_bluetooth(BluetoothAdapter bt_adapter) {
		
		_bt_adapter = bt_adapter;
		
	};		
	
	public void connectWithDevice(BluetoothDevice device) {
		
		BluetoothSocket tmp_socket = null;
		_bt_device = device;
		
		Log.d("Bluetooth", "Connecting with " + device.getName() + " " + device.getAddress());
		
		try {
			tmp_socket = _bt_device.createRfcommSocketToServiceRecord(MainActivity.BLUETOOTH_MMS_UUID);
		} catch (IOException e) {
			// TODO popup with error			
		}
		
		_bt_socket = tmp_socket;
		
		try {
			_bt_socket.connect();
		} catch (IOException e) {
			try {
				_bt_socket.close();
			} catch (IOException e1) {
				// popup error close
			}
			// popup error connection
		}
		
		// create thread for managing data exchange
		_bt_thread = new mms_bluetooth_device(_bt_socket);
		_bt_thread.start();
		
		// add data to map
		
		// popup message connection successful		
 
		
	}
	
	public void disconnectDevice(BluetoothDevice device) {
		// TODO implementation for many devices (remove from map)
		try {
			_bt_socket.close();
		} catch (IOException e) {
			// popup error close
		}
	}
	
	// Bluetooth device discovery
	public void scanBluetooth() {
		
		if (!_bt_adapter.startDiscovery())
			Log.d("Bluetooth", "Start discovery failed");
		else
			Log.d("Bluetooth", "Start discovery success");
		// TODO pop-up start scanning Bluetooth
		
	}
	
	public void stopBluetoothDiscovery() {
		
		if (_bt_adapter.isDiscovering()) {
			if (!_bt_adapter.cancelDiscovery())
				Log.d("Bluetooth", "Cancel discovery failed");
			else
				Log.d("Bluetooth", "Cancel discovery success");
		}
		else
			Log.d("Bluetooth", "No need for cancel discovery, already done");
	}
	
}
