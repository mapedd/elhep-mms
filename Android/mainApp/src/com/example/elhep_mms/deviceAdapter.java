package com.example.elhep_mms;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class deviceAdapter extends ArrayAdapter<Device_data>{

    Context context;
    int layoutResourceId;   
    //Weather data[] = null;
    ArrayList<Device_data> data;
   
    public deviceAdapter(Context context, int layoutResourceId, ArrayList<Device_data> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;        
        this.data = data;
    }
    

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        DeviceHolder holder = null;
       
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
           
            holder = new DeviceHolder();
            
            holder.txtConnType = (TextView)row.findViewById(R.id.txtConnType);
            holder.txtDeviceName = (TextView)row.findViewById(R.id.txtDeviceName);
            holder.txtDeviceIP = (TextView)row.findViewById(R.id.txtDeviceIP);
            
            //holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            //holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
           
            row.setTag(holder);
        }
        else
        {
            holder = (DeviceHolder)row.getTag();
        }
        
        Device_data device = data.get(position);        
        
        if (device != null) {       
        	
	       // Device_data device = data[position];
        	if (device.type == Device_data.CONNECTION_WIFI) {        	
        		holder.txtConnType.setText("Wifi");
        		
        		try {
    				String deviceName = device.JSON_data.getString("deviceName");
    				holder.txtDeviceName.setText(deviceName);
    			} catch (JSONException e) {
    				holder.txtDeviceName.setText("?");
    			}
        		
        	}
        	else if (device.type == Device_data.CONNECTION_BLUETOOTH) {
        		holder.txtConnType.setText("Bluetooth");
        		holder.txtDeviceName.setText(((BluetoothDevice)device.handler).getName());
        	}	        
	        
	        holder.txtDeviceIP.setText(device.IP);
	        
	        //holder.imgIcon.setImageResource(weather.icon);
        }
        return row;
    }
   
    static class DeviceHolder
    {
        //ImageView imgIcon;
        TextView txtConnType;
        TextView txtDeviceName;
        TextView txtDeviceIP;        
    }
}